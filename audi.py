import speech_recognition as sr
import openai
import gtts 
from playsound import playsound

r = sr.Recognizer()

# Replace YOUR_API_KEY with your OpenAI API key
openai.api_key = "TOKEN"

# задаем модель и промпт
model_engine = "text-davinci-003"


with sr.Microphone() as source:
    r.adjust_for_ambient_noise(source)
    data = r.record(source, duration=5)
    print("Sesinizi Tanımlıyor…")
    text = r.recognize_google(data,show_all=True,language="ru")
    print(text)
    transcript = text['alternative'][0]['transcript']
    print("=====================================================")
    
    
prompt = "Переведи на английский язык фразу - "+transcript

print(prompt)

# задаем макс кол-во слов
max_tokens = 128

# генерируем ответ
completion = openai.Completion.create(
    engine=model_engine,
    prompt=prompt,
    max_tokens=1024,
    temperature=0.5,
    top_p=1,
    frequency_penalty=0,
    presence_penalty=0
)

# выводим ответ
print(completion.choices[0].text)

# make a request to google to get synthesis
t1 = gtts.gTTS(completion.choices[0].text)

# save the audio file
t1.save("welcome.mp3")

# play the audio file
playsound("welcome.mp3")
